FROM openjdk:8-jre-alpine
MAINTAINER Mauron <gmacastil@gmail.com>
ADD target/*.jar demo.jar
ENTRYPOINT ["java", "-jar", "demo.jar"]
EXPOSE 9090